const Env = require("./natsukibot_enviroment.json");
const Discord = require("discord.js");
const {Intents} = require("discord.js");
const TimeConverter = require("./features/timeconverter.js");
const client = new Discord.Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.DIRECT_MESSAGES]});

let token = Env.TOKEN;

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on("messageCreate", msg => {
    if(msg.content === "!timezone") {
        TimeConverter.currentJSTandUTC()
            .then(function (value) {
                msg.reply(value);
            });
    }
});

console.log("test");
client.login(token);