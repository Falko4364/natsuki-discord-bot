const fetch = require('node-fetch');

let currentJSTandUTC = async function(){
    let response;
    await fetch("http://worldtimeapi.org/api/timezone/Asia/Tokyo")
        .then(response => response.json())
        .then(function(value){
            let rawDateUTC = value.utc_datetime.split('T');
            let dateUTC = rawDateUTC[0].substr(5).replace("-","/");
            let tempTimeUTC = value.utc_datetime.split('T')[1].split(".")[0];
            let timeUTC = tempTimeUTC.substr(tempTimeUTC.length - 8,tempTimeUTC.length - 3);
            let rawDateJST = value.datetime.split('T');
            let dateJST = rawDateJST[0].substr(5).replace("-","/");
            let tempTimeJST = value.datetime.split('T')[1].split(".")[0];
            let timeJST = tempTimeJST.substr(tempTimeJST.length - 8, tempTimeJST.length - 3);

            response = "```UTC : " + dateUTC + " " + timeUTC + "\nJST : " + dateJST + " " + timeJST + "```";
    });
    return response;
}

exports.currentJSTandUTC = currentJSTandUTC;


